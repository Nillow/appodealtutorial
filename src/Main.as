package {

import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.events.TouchEvent;
    import flash.text.TextField;
    import com.appodeal.aneplugin.*;

    public class Main extends Sprite
    {
        private var textField:TextField;

        private var appodeal:Appodeal = new Appodeal();
        private var button:Sprite;

        public function Main()
        {
            var appKey:String = "c7856090db7a75f87a60b40d3fdc565e85aa494b148d625c";
            appodeal.initialize(appKey, AdType.INTERSTITIAL | AdType.NON_SKIPPABLE_VIDEO);
//            appodeal.setTesting(true);


            textField = new TextField();
            textField.text = "Hello, World";
            textField.addEventListener(MouseEvent.CLICK, onTextClick);
            addChild(textField);


            button = new Sprite();
            button.graphics.beginFill(0x000000, 1);
            button.graphics.drawRect(0, 0, 200, 200);
            button.graphics.endFill();
            button.buttonMode = true;

            button.x = 100;
            button.y = 100;

            button.addEventListener(MouseEvent.CLICK, OnMouseCLick);

            addChild(button);
        }

        private function onTextClick(event:MouseEvent):void
        {
            textField.text = "On text clicked";
        }

        private function OnMouseCLick(event:MouseEvent):void
        {
            trace("On mouse clicked");
            button.graphics.beginFill(0x00ff00, 1);
            button.graphics.drawRect(0, 0, 200, 200);
            button.graphics.endFill();

            try
            {
                appodeal.show(AdType.INTERSTITIAL);
            }
            catch(e:Error)
            {
                textField.text = e.getStackTrace();
            }
            finally
            {
                textField.text = "On mouse clicked " + textField.text;
            }
        }

        private function OnTouch(event:TouchEvent):void
        {
            textField.text = "On btn touched";
            OnMouseCLick(null);
        }

        private function onTextTouch(event:TouchEvent):void
        {
            textField.text = "On text touched";
        }
    }
}
